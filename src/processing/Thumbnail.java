/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * as published by the Free Software Foundation. 
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or find 
 * it included in the project.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License,
 * a covered work must retain the producer line in every PDF that is created
 * or manipulated using iText.
 */
package processing;

import GUI.Home;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.datatransfer.DataFlavor;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.awt.image.ImagingOpException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import javax.accessibility.Accessible;
import javax.activation.MimetypesFileTypeMap;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.TransferHandler;
import org.imgscalr.Scalr;

/**
 *
 * @author Mordechai
 */
public class Thumbnail extends JComponent implements MouseListener, FocusListener, Accessible, MouseMotionListener, Comparable<Thumbnail>
{

    public static enum FileType
    {

        image, pdf
    };

    private FileType type;
    private String fileName;
    private String fileExtension;
    private Image image;
    private int index;

    private BufferedImage thumbnail;
    public static int THUMB_WIDTH = 150;
    public static int THUMB_HEIGHT = 150;
    public static DataFlavor[] DATA_FLAVOURS =
    {
        DataFlavor.imageFlavor
    };
    private MouseEvent firstMouseEvent = null;

    public Thumbnail(FileType type, String fileName, int index) throws IOException
    {
        this.index = index;
        this.fileName = fileName;
        this.type = type;
        if (type == FileType.image)
        {
            this.image = createImageIcon(fileName).getImage();
        } else if (type == FileType.pdf)
        {
            this.image = getImageOfPDF(fileName);
        }

        fileExtension = getFileExtension(fileName);

        setFocusable(true);
        addMouseListener(this);
        addMouseMotionListener(this);
        addFocusListener(this);

        if (image != null)
        {
            setThumbnail();
        }

        // setPreferredSize(new Dimension(125, 125));
    }

    public static String getFileExtension(String fileName)
    {
        String[] split = fileName.split("\\.(?=[^\\.]+$)");
        return split[1];
    }

    public static String getFileExtension(File file)
    {
        return getFileExtension(file.getName());
    }

    /**
     * Returns an ImageIcon, or null if the path was invalid.
     */
    private ImageIcon createImageIcon(String imageName)
    {
        return new ImageIcon(Home.getShrinkFilePath(imageName), imageName);
    }

    private Image getImageOfPDF(String pdfName) throws FileNotFoundException, IOException
    {
        File pdfFile = new File(Home.getShrinkFilePath(pdfName));
        RandomAccessFile raf = new RandomAccessFile(pdfFile, "r");
        FileChannel channel = raf.getChannel();
        MappedByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
        PDFFile pdf = new PDFFile(buf);
        PDFPage page = pdf.getPage(0);

        // create the image
        Rectangle rect = new Rectangle(0, 0, (int) page.getBBox().getWidth(),
                (int) page.getBBox().getHeight());

        Image pdfImage = page.getImage(rect.width, rect.height, // width & height
                rect, // clip rect
                null, // null for the ImageObserver
                true, // fill background with white
                true // block until drawing is done
        );

        raf.close();
        channel.close();

        return pdfImage;
    }

    public void createPdfOfThumbnail()
    {
        Document document = new Document();

        try
        {
            PdfWriter.getInstance(document, new FileOutputStream(getNameOfPdfOfThumbnail()));
            document.open();

            com.itextpdf.text.Image itextImage = com.itextpdf.text.Image.getInstance(Home.getShrinkFilePath(fileName));
            itextImage.scaleToFit(new com.itextpdf.text.Rectangle(document.left(), document.bottom(), document.right(), document.top()));
            document.add(itextImage);

            document.close();
        } catch (DocumentException | IOException e)
        {
            e.printStackTrace();
        }
    }

    public String getNameOfPdfOfThumbnail()
    {
        return Home.getShrinkFilePath(fileName) + ".pdf";
    }

    public void setImage(Image image, String imageName)
    {
        this.image = image;
        this.fileName = imageName;
        String[] split = imageName.split("\\.");
        fileExtension = split[1];

        setThumbnail();
        this.repaint();
    }

    public FileType getType()
    {
        return type;
    }

    public void setFileType(FileType type)
    {
        this.type = type;
    }

    public int getIndex()
    {
        return index;
    }

    public void setIndex(int index)
    {
        this.index = index;
    }

    public static FileType determineFileType(String filePath)
    {
        return determineFileType(new File(filePath));
    }

    public static FileType determineFileType(File file)
    {
        String mimetype = new MimetypesFileTypeMap().getContentType(file);
        String type = mimetype.split("/")[0];
        System.out.println(mimetype);
        if (type.equals("image"))
        {
            return FileType.image;
        } else if (mimetype.equals("application/pdf") || mimetype.equals("application/x-pdf"))
        {
            return FileType.pdf;
        } else if (mimetype.equals("application/octet-stream"))
        {
            // some image files are not seen as images for some reason. seen only for png so far. so do this as last ditch
            if (getFileExtension(file).equalsIgnoreCase("png") || getFileExtension(file).equalsIgnoreCase("jpg") || getFileExtension(file).equalsIgnoreCase("jpeg") || getFileExtension(file).equalsIgnoreCase("gif"))
            {
                return FileType.image;
            } else if (getFileExtension(file).equalsIgnoreCase("pdf"))
            {
                return FileType.pdf;
            }
        }
        return null;
    }

    public Image getImage()
    {
        return image;
    }

    public String getFileName()
    {
        return fileName;
    }

    public String getFileExtension()
    {
        return fileExtension;
    }

    private void setThumbnail()
    {
        try
        {
            BufferedImage bimage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_RGB);

            // Draw the image on to the buffered image
            Graphics2D bGr = bimage.createGraphics();
            bGr.drawImage(image, 0, 0, null);
            bGr.dispose();
            thumbnail = Scalr.resize(bimage, Scalr.Method.SPEED, Scalr.Mode.FIT_EXACT, THUMB_WIDTH, THUMB_HEIGHT);
            bimage.flush();
            
        } catch (IllegalArgumentException | ImagingOpException e)
        {
            System.out.println("FAIL MAKING THUMBNAIL + " + e);
            e.printStackTrace();
        }
    }

    @Override
    public void paintComponent(Graphics graphics)
    {

        Graphics g = graphics.create();

        //Draw in our entire space, even if isOpaque is false.
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, thumbnail == null ? THUMB_WIDTH : thumbnail.getWidth(), thumbnail == null ? THUMB_HEIGHT : thumbnail.getHeight());

        if (image != null)
        {

            //Scalr.resize(image, Scalr.Method.SPEED, Scalr.Mode.FIT_TO_WIDTH, 150, 100, Scalr.OP_ANTIALIAS);
            g.drawImage(thumbnail, 0, 0, this);
        }

        //Add a border, red if picture currently has focus
//        if (isFocusOwner())
//        {
//            g.setColor(Color.RED);
//        } else
//        {
//            g.setColor(Color.BLACK);
//        }
//        g.drawRect(0, 0, thumbnail == null ? THUMB_WIDTH : thumbnail.getWidth(), thumbnail == null ? THUMB_HEIGHT : thumbnail.getHeight());
        g.dispose();

    }

    @Override
    public void mouseClicked(MouseEvent e)
    {
        requestFocusInWindow();
    }

    @Override
    public void mousePressed(MouseEvent e)
    {
        //Don't bother to drag if there is no image.
        if (image == null)
        {
            return;
        }

        firstMouseEvent = e;
        e.consume();
    }

    @Override
    public void mouseReleased(MouseEvent e)
    {
        firstMouseEvent = null;
    }

    @Override
    public void mouseEntered(MouseEvent e)
    {

    }

    @Override
    public void mouseExited(MouseEvent e)
    {

    }

    @Override
    public void focusGained(FocusEvent e)
    {
        this.repaint();
    }

    @Override
    public void focusLost(FocusEvent e)
    {
        this.repaint();
    }

    @Override
    public void mouseDragged(MouseEvent e)
    {
        //Don't bother to drag if the component displays no image.
        if (image == null)
        {
            return;
        }

        if (firstMouseEvent != null)
        {
            e.consume();

            //If they are holding down the control key, COPY rather than MOVE
            // int ctrlMask = InputEvent.CTRL_DOWN_MASK;
            //    int action = ((e.getModifiersEx() & ctrlMask) == ctrlMask) ? TransferHandler.COPY : TransferHandler.MOVE;
            int action = TransferHandler.MOVE;// limit to move

            int dx = Math.abs(e.getX() - firstMouseEvent.getX());
            int dy = Math.abs(e.getY() - firstMouseEvent.getY());
            //Arbitrarily define a 5-pixel shift as the
            //official beginning of a drag.
            if (dx > 5 || dy > 5)
            {
                //This is a drag, not a click.
                JComponent c = (JComponent) e.getSource();
                TransferHandler handler = c.getTransferHandler();
                //Tell the transfer handler to initiate the drag.
                handler.exportAsDrag(c, firstMouseEvent, action);
                firstMouseEvent = null;
            }
        }
    }

    @Override
    public void mouseMoved(MouseEvent e)
    {

    }

    @Override
    public int compareTo(Thumbnail other)
    {
        return this.getIndex() > other.getIndex() ? 1 : this.getIndex() < other.getIndex() ? -1 : 0;
    }

}
