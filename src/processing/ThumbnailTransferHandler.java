/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * as published by the Free Software Foundation. 
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or find 
 * it included in the project.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License,
 * a covered work must retain the producer line in every PDF that is created
 * or manipulated using iText.
 */
package processing;

import java.awt.Image;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.TransferHandler;
import static javax.swing.TransferHandler.COPY_OR_MOVE;
import static javax.swing.TransferHandler.MOVE;

/**
 *
 * @author Mordechai
 */
public class ThumbnailTransferHandler extends TransferHandler
{

    private Thumbnail sourceThumb;
    private Image destinationImage;
    private Thumbnail.FileType destinationType;
    private String destinationFileName;

    boolean shouldRemove;

    @Override
    public boolean importData(JComponent c, Transferable t)
    {
        Image image;

        if (canImport(c, t.getTransferDataFlavors()))
        {
            Thumbnail destinationThumb = (Thumbnail) c;
            //Don't drop on myself.
            if (sourceThumb == destinationThumb)
            {
                shouldRemove = false;
                return true;
            }
            try
            {
                image = (Image) t.getTransferData(DataFlavor.imageFlavor);

                //Set the component to the new picture.
                destinationImage = destinationThumb.getImage();
                destinationFileName = destinationThumb.getFileName();
                destinationType = destinationThumb.getType();

                destinationThumb.setImage(image, sourceThumb.getFileName());
                destinationThumb.setFileType(sourceThumb.getType());

                return true;
            } catch (UnsupportedFlavorException ufe)
            {
                System.out.println("importData: unsupported data flavor");
            } catch (IOException ioe)
            {
                System.out.println("importData: I/O exception");
            }
        }
        return false;
    }

    @Override
    protected Transferable createTransferable(JComponent c)
    {
        sourceThumb = (Thumbnail) c;
        shouldRemove = true;
        return new PictureTransferable(sourceThumb);
    }

    @Override
    public int getSourceActions(JComponent c)
    {
        return COPY_OR_MOVE;
    }

    @Override
    protected void exportDone(JComponent c, Transferable data, int action)
    {
        if (shouldRemove && (action == MOVE) && destinationImage != null)
        {
            sourceThumb.setImage(destinationImage, destinationFileName);
            sourceThumb.setFileType(destinationType);
        }
        sourceThumb = null;
        destinationImage = null;
        destinationFileName = null;
        destinationType = null;
    }

    @Override
    public boolean canImport(JComponent c, DataFlavor[] flavors)
    {
        for (DataFlavor flavor : flavors)
        {
            for (DataFlavor dataFlavour : Thumbnail.DATA_FLAVOURS)
            {
                if (dataFlavour.equals(flavor))
                {
                    return true;
                }
            }

        }
        return false;
    }

}
