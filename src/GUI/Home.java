/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * as published by the Free Software Foundation. 
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or find 
 * it included in the project.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License,
 * a covered work must retain the producer line in every PDF that is created
 * or manipulated using iText.
 */
package GUI;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.filechooser.FileFilter;
import org.imgscalr.Scalr;
import preferences.PreferencesManager;
import processing.Thumbnail;
import processing.ThumbnailTransferHandler;

/**
 *
 * @author Mordechai
 */
public class Home extends javax.swing.JFrame implements PropertyChangeListener
{

    public enum CopyToLocation
    {

        SAVE_FOLDER, PROCCESSED_FOLDER
    };

    private LoadAndDisplayExistingFilesTask loadExisitingImagesTask;
    private int numFiles = 0;
    private boolean isPdfToProccess = false;

    private static File saveFolder;
    private static File saveProccessedFilesFolder;

    private ArrayList<Thumbnail> thumbnails = new ArrayList<>();
    private ThumbnailTransferHandler transferHandler = new ThumbnailTransferHandler();

    private ArrayList<String> fileNames = new ArrayList<String>();

    private int num_cols;
    private int num_rows;
    public final static int HORIZONTAL_DISPLACMENT = Thumbnail.THUMB_WIDTH + 20;
    public final static int VERTICAL_DISPLACMENT = Thumbnail.THUMB_HEIGHT + 20;

    public static final String COMPRESS_ZIP_COMPLETE_MSG = "Compress and zip of files completed. Zip is located at: ";
    public static final String COMPRESS_ZIP_COMPLETE_TITLE = "Compress and Zip Complete";
    public static final String MERGE_PDF_COMPLETE_MSG = "Merge of files to PDF completed. PDF is located at: ";
    public static final String MERGE_PDF_COMPLETE_TITLE = "Merge to PDF Complete";

    public static final String MERGE_PDF_COMPLETE_MSG_WITH_IMAGES = "Images resized and merge of files to PDF completed. PDF is located at: ";
    public static final String MERGE_PDF_COMPLETE_WITH_IMAGES_TITLE = "Complete";

    public static final String IMAGE_RESIZEING_COMPLETE = "Images resizing completed.";
    public static final String MERGE_IMAGES_COMPLETE_TITLE = "Resizing complete";

    public static final String MERGED_PDF_NAME = "MergedPDF";
    public static final String CANCEL_DONE_COMMAND_STRING = "12341234123jkhskdjfhkjdj/djkjhui34093809";

    public static final String RESIZE_METHOD_AUTOMATIC = "Maintain proportions";
    public static final String RESIZE_METHOD_FIT_WIDTH = "Fit to given width";
    public static final String RESIZE_METHOD_FIT_HEIGHT = "Fit to given height";
    public static final String RESIZE_METHOD_FIT_EXACTLY = "Fit exactly to given dimensions";

    private static DateFormat df = new SimpleDateFormat("yy-MM-dd_HH-mm");
    private PreferencesManager prefMan;

    /**
     * Creates new form Home
     */
    public Home()
    {
        try
        {
            //UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex)
        {
            System.out.println("Problem setting look and feel.");
        }

        initComponents();
        initProgressBar();

        resizeMethodCombo.addItem(new ResizeMethodComboItem(RESIZE_METHOD_AUTOMATIC, Scalr.Mode.AUTOMATIC));
        resizeMethodCombo.addItem(new ResizeMethodComboItem(RESIZE_METHOD_FIT_HEIGHT, Scalr.Mode.FIT_TO_HEIGHT));
        resizeMethodCombo.addItem(new ResizeMethodComboItem(RESIZE_METHOD_FIT_WIDTH, Scalr.Mode.FIT_TO_WIDTH));
        resizeMethodCombo.addItem(new ResizeMethodComboItem(RESIZE_METHOD_FIT_EXACTLY, Scalr.Mode.FIT_EXACT));

        prefMan = new PreferencesManager();
        loadPreferences();
        addAllInZipCheckBox.setVisible(false);

        Runtime.getRuntime().addShutdownHook(new Thread()
        {
            @Override
            public void run()
            {
                savePreferences();
            }
        });

        prefMan.setSaveFolderToCurrentPrefs();
        prefMan.setProccessedFolderTOCurrentPrefs();

        initDragAndDrop(this);

        loadExisitingImagesTask = new LoadAndDisplayExistingFilesTask();
        loadExisitingImagesTask.addPropertyChangeListener(this);
        loadExisitingImagesTask.execute();

        //use this to find new size and determine how many imageThumbnails we can have in rows and cols
        this.addComponentListener(new ComponentListener()
        {
            @Override
            public void componentResized(ComponentEvent evt)
            {
                arrangeThumbnails();
            }

            @Override
            public void componentMoved(ComponentEvent e)
            {
            }

            @Override
            public void componentShown(ComponentEvent e)
            {
            }

            @Override
            public void componentHidden(ComponentEvent e)
            {
            }
        });

    }

    @Override
    public Dimension preferredSize()
    {
        return super.preferredSize(); //To change body of generated methods, choose Tools | Templates.
    }

    private void loadPreferences()
    {
        addAllInZipCheckBox.setSelected(prefMan.isAddAllToZipPref());
        mergeImagesCheckBox.setSelected(prefMan.isMergeImagesPref());
        resizeImagesCheckBox.setSelected(prefMan.isResizeImagesPref());
        zipFilesCheckBox.setSelected(prefMan.isZipFilesPref());
        addPageNumbersCheckBox.setSelected(prefMan.isAddAllToZipPref());
        resizeToHeightT.setText(prefMan.getImageHeightPref() + "");
        resizeToWidthT.setText(prefMan.getImageWidthPref() + "");
    }

    public static void setSaveFile(File file)
    {
        saveFolder = file;
    }

    public static void setProcessedFilesFile(File file)
    {
        saveProccessedFilesFolder = file;
    }

    private void savePreferences()
    {
        int height = getEnteredImageResizeHeight();

        int width = getEnteredImageResizeWidth();

        prefMan.setPreferences(mergeImagesCheckBox.isSelected(), resizeImagesCheckBox.isSelected(), addAllInZipCheckBox.isSelected(), zipFilesCheckBox.isSelected(), addPageNumbersCheckBox.isSelected(), width, height);
    }

    private void initProgressBar()
    {
        progressBar.setValue(0);
        progressBar.setVisible(false);
        progressBarLable.setVisible(false);
        progressBar.setStringPainted(true);
    }

    private void showProgressBar(int progMax, String labelText)
    {

        Runnable runnable = () ->
        {
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            progressBar.setVisible(true);
            progressBar.setMaximum(progMax);
            progressBarLable.setVisible(true);
            progressBarLable.setText(labelText);
        };

        SwingUtilities.invokeLater(runnable);

    }

    private void updateNumFilesLabel(String text)
    {
        Runnable runnable = () ->
        {
            numFilesLabel.setText(text + " files.");
        };

        SwingUtilities.invokeLater(runnable);
    }

    private void updateNumFilesLabel(int num)
    {
        numFiles = num;
        updateNumFilesLabel(num + "");
    }

    private void hideProgressBar()
    {
        Runnable runnable = () ->
        {
//            if (!SwingUtilities.isEventDispatchThread())
//            {
//                throw new RuntimeException("This method can only be run on the EDT");
//            }
            setCursor(null);
            progressBar.setVisible(false);
            progressBarLable.setVisible(false);
            arrangeThumbnails();

        };

        SwingUtilities.invokeLater(runnable);
    }

    private void showDialog(String message, String title, int type)
    {
        Runnable runnable = () ->
        {
            try
            {
                JOptionPane.showMessageDialog(null, message, title, type);
            } catch (Exception ex)
            {
                JOptionPane.showMessageDialog(null, message, title, JOptionPane.INFORMATION_MESSAGE);
            }
        };

        SwingUtilities.invokeLater(runnable);

    }

    /**
     * Invoked when task's progress property changes.
     *
     * @param evt
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt)
    {
        if ("progress".equals(evt.getPropertyName()))
        {
            int progress = (Integer) evt.getNewValue();
            progressBar.setValue(progress);
            // taskOutput.append(String.format(
            //          "Completed %d%% of task.\n", task.getProgress()));
        }
    }

    private int findExistingFiles()
    {
        File[] listOfFiles = saveFolder.listFiles();
        System.out.println("Existing files:");
        for (int i = 0; i < listOfFiles.length; i++)
        {
            if (listOfFiles[i].isFile())
            {

                if (Thumbnail.determineFileType(listOfFiles[i]) != null)
                {
                    fileNames.add(listOfFiles[i].getName());
                    System.out.println("Added: " + listOfFiles[i].getName());
                }
                System.out.println(listOfFiles[i].getName());
            }
        }
        return listOfFiles.length;
    }

    private void addThumbnail(Thumbnail.FileType type, String imageName, int index)
    {
        try
        {
            Thumbnail img = new Thumbnail(type, imageName, index);
            img.setTransferHandler(transferHandler);

            thumbnails.add(img);
            fileLayer.add(img, 0);
        } catch (IndexOutOfBoundsException | IOException ex)
        {
            System.out.println("Could not add " + imageName);
            ex.printStackTrace();
        }

        arrangeThumbnails();
    }

    private void arrangeThumbnails()
    {
        setRowsCols();
        fileLayer.setPreferredSize(new Dimension(HORIZONTAL_DISPLACMENT * num_cols + Thumbnail.THUMB_WIDTH, HORIZONTAL_DISPLACMENT * num_rows + Thumbnail.THUMB_HEIGHT));

        for (int i = 0; i < num_rows; i++)
        {

            for (int j = 0; j < num_cols; j++)
            {
                try
                {
                    Thumbnail img = (Thumbnail) thumbnails.get((num_cols * i) + j);
                    img.setBounds(j * VERTICAL_DISPLACMENT, i * HORIZONTAL_DISPLACMENT, Thumbnail.THUMB_WIDTH, Thumbnail.THUMB_HEIGHT);
                } catch (IndexOutOfBoundsException ex)
                {
                    break;
                }

            }

        }

        imagePanelScrollPane.revalidate();

    }

    private void setRowsCols()
    {
        double scrollerWidth = imagePanelScrollPane.getWidth();

        num_cols = (int) Math.floor(scrollerWidth / (double) HORIZONTAL_DISPLACMENT);
        num_rows = (int) Math.ceil((double) fileNames.size() / (double) num_cols);
    }

    public static String getShrinkFilePath(String fileName)
    {
        return saveFolder.getAbsolutePath() + File.separator + fileName;
    }

    public static String getProcessedFilePath(String fileName)
    {
        return saveProccessedFilesFolder.getAbsolutePath() + File.separator + fileName;
    }

    private void initDragAndDrop(PropertyChangeListener pcl)
    {
        this.setDropTarget(new DropTarget()
        {
            @Override
            public synchronized void drop(DropTargetDropEvent dtde)
            {
                try
                {
                    Transferable transfer = dtde.getTransferable();
                    if (transfer.isDataFlavorSupported(DataFlavor.javaFileListFlavor))
                    {
                        dtde.acceptDrop(DnDConstants.ACTION_COPY);
                        List droppedElements = (List) transfer.getTransferData(DataFlavor.javaFileListFlavor);

                        CopyTask ct = new CopyTask(droppedElements, CopyToLocation.SAVE_FOLDER);
                        ct.addPropertyChangeListener(pcl);
                        ct.execute();

                    } else
                    {
                        System.err.println("Data flavor not supported.");
                    }
                } catch (UnsupportedFlavorException ex)
                {
                    System.err.println(ex.getMessage());
                    ex.printStackTrace();
                } catch (IOException ex)
                {
                    System.err.println(ex.getMessage());
                    ex.printStackTrace();
                } catch (Exception ex)
                {
                    System.err.println(ex.getMessage());
                    ex.printStackTrace();
                } finally
                {
                    dtde.dropComplete(true);

                }
            }
        });
    }

    /**
     *
     * @author Mordechai
     */
    class CopyTask extends SwingWorker<Void, Void>
    {

        private List<Object> elements;
        private CopyToLocation destination;

        public CopyTask(List<Object> elements, CopyToLocation destination)
        {
            this.elements = elements;
            this.destination = destination;
        }

        @Override
        public Void doInBackground()
        {
            showProgressBar(elements.size(), "Copying files...");
            int done = 0;
            int index = 0;
//imagePanelScrollPane.getLocation() //teaches us to use component listeners for memory managment
            for (Object element : elements)
            {
                if (element instanceof File)
                {
                    File source = (File) element;

                    Thumbnail.FileType sourceType = Thumbnail.determineFileType(source);
                    if (sourceType == Thumbnail.FileType.pdf)
                    {
                        isPdfToProccess = true;
                    }

                    if (sourceType != null)
                    {
                        try
                        {
                            File dest;
                            if (destination == CopyToLocation.SAVE_FOLDER)
                            {
                                dest = new File(getShrinkFilePath(source.getName()));
                            } else if (destination == CopyToLocation.PROCCESSED_FOLDER)
                            {
                                dest = new File(getProcessedFilePath(source.getName()));
                            } else
                            {
                                throw new IllegalArgumentException();
                            }

                            Files.copy(Paths.get(source.getAbsolutePath()), Paths.get(dest.getAbsolutePath()), StandardCopyOption.REPLACE_EXISTING);

                            if (destination == CopyToLocation.SAVE_FOLDER)
                            {
                                fileNames.add(source.getName());

                                addThumbnail(sourceType, source.getName(), index);

                                index++;
                            }

                            System.out.println("File copied from " + source.getAbsolutePath() + " to " + dest.getAbsolutePath());
                        } catch (IOException ex)
                        {
                            System.err.println("Error: " + ex);
                        }
                    } else
                    {
                        System.err.println("FILE TYPE UNKNOWN");
                    }
                    done++;
                    setProgress(done);

                }
            }

            return null;
        }
        /*
         * Executed in event dispatching thread
         */

        @Override
        public void done()
        {
            hideProgressBar();

            try
            {
                if (!isCancelled())
                {
                    get();

                    if (destination == CopyToLocation.PROCCESSED_FOLDER)
                    {
                        removeAllFiles();
                    }
                    updateNumFilesLabel(fileNames.size());
                }
            } catch (ExecutionException e)
            {
                System.out.println("Exception: " + e.getCause());
                showDialog("Error loading files. This is commonly caused by too many files or files that are too large. Error: " + e.getCause(), "Error", JOptionPane.ERROR_MESSAGE);
                removeAllFiles();
            } catch (InterruptedException e)
            {
                System.out.println("Exception: " + e.getCause());
                showDialog("Error loading files. This is commonly caused by too many files or files that are too large. Error: " + e.getCause(), "Error", JOptionPane.ERROR_MESSAGE);
                removeAllFiles();
            }

        }

    }

    /**
     *
     * @author Mordechai
     */
    class LoadAndDisplayExistingFilesTask extends SwingWorker<Void, Void>
    {

        public LoadAndDisplayExistingFilesTask()
        {

        }

        @Override
        public Void doInBackground()
        {
            int done = 0;
            int numFound = 0;
            try
            {
                numFound = findExistingFiles();

                showProgressBar(fileNames.size(), "Loading files...");

                setRowsCols();
                fileLayer.setPreferredSize(new Dimension(HORIZONTAL_DISPLACMENT * num_cols + Thumbnail.THUMB_WIDTH, HORIZONTAL_DISPLACMENT * num_rows + Thumbnail.THUMB_HEIGHT));

                for (int i = 0; i < num_rows; i++)
                {

                    for (int j = 0; j < num_cols; j++)
                    {
                        try
                        {
                            Thumbnail.FileType type = Thumbnail.determineFileType(getShrinkFilePath(fileNames.get((num_cols * i) + j)));
                            if (type == Thumbnail.FileType.pdf)
                            {
                                isPdfToProccess = true;
                            }
                            if (type != null)
                            {
                                Thumbnail img = new Thumbnail(type, fileNames.get((num_cols * i) + j), (num_cols * i) + j);
                                img.setTransferHandler(transferHandler);

                                img.setBounds(j * VERTICAL_DISPLACMENT, i * HORIZONTAL_DISPLACMENT, Thumbnail.THUMB_WIDTH, Thumbnail.THUMB_HEIGHT);
                                thumbnails.add(img);
                                fileLayer.add(img, 0);
                            } else
                            {
                                System.err.println("Unknown file type");
                            }
                            done++;
                            //System.out.println("DONE: " + done + " " + ((int) Math.ceil(100 * done / progressBar.getMaximum())) + "% " + (100 * done / progressBar.getMaximum()));
                            setScaledProgress(done);

                        } catch (IndexOutOfBoundsException ex)
                        {
                            break;
                        } catch (IOException | IllegalArgumentException ex)
                        {
                            //skip the fail file.
                        }

                    }

                }
            } catch (Exception ex)
            {
                System.err.println("Error with loading existing files:");
                //ex.printStackTrace();
            }

            updateNumFilesLabel(fileNames.size());
            return null;
        }

        private void setScaledProgress(int completed)
        {
            setProgress(100 * completed / progressBar.getMaximum());
        }

        /*
         * Executed in event dispatching thread
         */
        @Override
        public void done()
        {
            hideProgressBar();
            try
            {
                if (!isCancelled())
                {
                    get();
                }
            } catch (ExecutionException e)
            {
                System.out.println("Exception: " + e.getCause());
                showDialog("Error loading files. This is commonly caused by too many files or files that are too large.  Error: " + e.getCause(), "Error", JOptionPane.ERROR_MESSAGE);
                removeAllFiles();
            } catch (InterruptedException e)
            {
                System.out.println("Exception: " + e.getCause());
                showDialog("Error loading files. This is commonly caused by too many files or files that are too large.  Error: " + e.getCause(), "Error", JOptionPane.ERROR_MESSAGE);
                removeAllFiles();
            }

        }

    }

    /**
     *
     * @author Mordechai
     */
    class ProccessFilesTask extends SwingWorker<Void, Void>
    {

        String pdfName = "";
        boolean processPdfs = true;

        boolean wasImage = false;

        public ProccessFilesTask(String pdfName)
        {
            this.pdfName = pdfName;
        }

        public ProccessFilesTask(boolean processPdfs)
        {
            if (processPdfs)
            {
                this.pdfName = getPDFName(true);//will use default name
            }
            this.processPdfs = processPdfs;
        }

        @Override
        public Void doInBackground()
        {
            Collections.sort(thumbnails);

            thumbnails.stream().forEach((thumbnail) ->
            {
                System.out.println(thumbnail.getFileName() + " " + thumbnail.getIndex());
            });

            List<InputStream> pdfs = new ArrayList<InputStream>();
            OutputStream output = null;

            if (processPdfs)
            {
                try
                {
                    output = new FileOutputStream(pdfName);
                } catch (FileNotFoundException ex)
                {
                    System.err.println("could not create merge pdf output stream");
                }
            }

            showProgressBar(fileNames.size(), "Processing files...");
            int done = 0;
            for (Thumbnail thumbnail : thumbnails)
            {
                if (thumbnail.getType() == Thumbnail.FileType.image)
                {
                    wasImage = true;
                    if (resizeImagesCheckBox.isSelected())
                    {
                        //System.out.println("trying to resize image");
                        BufferedImage img;
                        if (thumbnail.getFileExtension().equalsIgnoreCase("png") || thumbnail.getFileExtension().equalsIgnoreCase("gif"))
                        {
                            img = toBufferedImage(thumbnail.getImage(), BufferedImage.TYPE_INT_ARGB);
                        } else
                        {
                            img = toBufferedImage(thumbnail.getImage(), BufferedImage.TYPE_INT_RGB);
                        }

                        BufferedImage scaledImg = Scalr.resize(img, Scalr.Method.ULTRA_QUALITY, ((ResizeMethodComboItem) resizeMethodCombo.getSelectedItem()).getMode(), getEnteredImageResizeWidth(), getEnteredImageResizeHeight());
                        File scaledImgFile = new File(getShrinkFilePath(thumbnail.getFileName()));
                        try
                        {
                            ImageIO.write(scaledImg, thumbnail.getFileExtension(), scaledImgFile);
                            done++;
                            setScaledProgress(done);
                        } catch (IOException ex)
                        {
                            System.out.println("Image: " + thumbnail.getFileName() + " could not be shrunk.");
                        }
                    }
                    if (mergeImagesCheckBox.isSelected())
                    {
                        try
                        {
                            thumbnail.createPdfOfThumbnail();
                            pdfs.add(new FileInputStream(thumbnail.getNameOfPdfOfThumbnail()));
                        } catch (FileNotFoundException ex)
                        {
                            Logger.getLogger(Home.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                } else if (thumbnail.getType() == Thumbnail.FileType.pdf)
                {
                    try
                    {
                        pdfs.add(new FileInputStream(getShrinkFilePath(thumbnail.getFileName())));
                        done++;
                        setScaledProgress(done);
                    } catch (FileNotFoundException ex)
                    {
                        System.err.println("Could not add pdf to stream. Stack trace:");
                        Logger.getLogger(Home.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            }

            if (output != null && processPdfs)
            {
                concatPDFs(pdfs, output, addPageNumbersCheckBox.isSelected());
            } else
            {
                System.out.println("Could not create PDF to merge to.");
            }

            //scrollpane revalidation done in done()
            return null;
        }

        private void setScaledProgress(int completed)
        {
            setProgress(100 * completed / progressBar.getMaximum());
        }

        public void concatPDFs(List<InputStream> streamOfPDFFiles, OutputStream outputStream, boolean paginate)
        {
            if (streamOfPDFFiles.size() > 0)
            {
                Document document = new Document()
                {
                };
                try
                {
                    List<InputStream> pdfs = streamOfPDFFiles;
                    List<PdfReader> readers = new ArrayList<PdfReader>();
                    int totalPages = 0;
                    Iterator<InputStream> iteratorPDFs = pdfs.iterator();

                    // Create Readers for the pdfs.
                    while (iteratorPDFs.hasNext())
                    {
                        InputStream pdf = iteratorPDFs.next();
                        PdfReader pdfReader = new PdfReader(pdf);
                        readers.add(pdfReader);
                        totalPages += pdfReader.getNumberOfPages();
                    }
                    // Create a writer for the outputstream
                    PdfWriter writer = PdfWriter.getInstance(document, outputStream);
                    writer.setFullCompression();

                    document.open();
                    BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA,
                            BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    PdfContentByte cb = writer.getDirectContent(); // Holds the PDF
                    // data

                    PdfImportedPage page;
                    int currentPageNumber = 0;
                    int pageOfCurrentReaderPDF = 0;
                    Iterator<PdfReader> iteratorPDFReader = readers.iterator();

                    // Loop through the PDF files and add to the output.
                    while (iteratorPDFReader.hasNext())
                    {
                        PdfReader pdfReader = iteratorPDFReader.next();

                        // Create a new page in the target for each source page.
                        while (pageOfCurrentReaderPDF < pdfReader.getNumberOfPages())
                        {
                            document.newPage();
                            pageOfCurrentReaderPDF++;
                            currentPageNumber++;
                            page = writer.getImportedPage(pdfReader, pageOfCurrentReaderPDF);
                            cb.addTemplate(page, 0, 0);

                            // Code for pagination.
                            if (paginate)
                            {
                                cb.beginText();
                                cb.setFontAndSize(bf, 9);
                                cb.showTextAligned(PdfContentByte.ALIGN_CENTER, "" + currentPageNumber + " of " + totalPages, 520, 5, 0);
                                cb.endText();
                            }
                        }
                        pageOfCurrentReaderPDF = 0;
                    }
                    outputStream.flush();
                    document.close();
                    outputStream.close();
                    writer.close();
                } catch (Exception e)
                {
                    e.printStackTrace();
                } finally
                {
                    if (document.isOpen())
                    {
                        document.close();
                    }
                    try
                    {
                        if (outputStream != null)
                        {
                            outputStream.close();
                        }
                    } catch (IOException ioe)
                    {
                        ioe.printStackTrace();
                    }
                }
            }
        }

        /*
         * Executed in event dispatching thread
         */
        @Override
        public void done()
        {
            hideProgressBar();

            try
            {
                if (!isCancelled())
                {
                    get();
                    // imagePanelScrollPane.revalidate();
                    ArrayList<File> files = new ArrayList<File>();
                    fileNames.stream().forEach((imageName) ->
                    {
                        files.add(new File(getShrinkFilePath(imageName)));
                    });

                    if (processPdfs && wasImage)
                    {
                        showDialog(MERGE_PDF_COMPLETE_MSG + "\n" + pdfName, MERGE_PDF_COMPLETE_TITLE, JOptionPane.INFORMATION_MESSAGE);
                    } else if (processPdfs && !wasImage)
                    {
                        showDialog(MERGE_PDF_COMPLETE_MSG_WITH_IMAGES + "\n" + pdfName, MERGE_PDF_COMPLETE_WITH_IMAGES_TITLE, JOptionPane.INFORMATION_MESSAGE);
                    } else if (!processPdfs && wasImage)
                    {
                        showDialog(IMAGE_RESIZEING_COMPLETE + "\n" + pdfName, MERGE_IMAGES_COMPLETE_TITLE, JOptionPane.INFORMATION_MESSAGE);
                    }

                    if (zipFilesCheckBox.isSelected())
                    {
                        zipFiles(files);
                    } else
                    {
                        moveFilesToProcessedFolder();
                    }
                }
            } catch (ExecutionException e)
            {
                // Exception occurred, deal with it
                System.out.println("Exception: " + e.getCause());
                showDialog("Error processing files. Please restart the application.", "Error: " + e.getCause(), JOptionPane.ERROR_MESSAGE);
            } catch (InterruptedException e)
            {
                showDialog("Error processing files. Please restart the application.", "Error: " + e.getCause(), JOptionPane.ERROR_MESSAGE);

            }

        }

    }

    private int getEnteredImageResizeHeight()
    {
        int height;
        try
        {
            height = Integer.parseInt(resizeToHeightT.getText());
        } catch (NumberFormatException ex)
        {
            height = prefMan.IMAGE_HEIGHT_DEFAULT;
        }
        return height;
    }

    private int getEnteredImageResizeWidth()
    {
        int width;
        try
        {
            width = Integer.parseInt(resizeToWidthT.getText());
        } catch (NumberFormatException ex)
        {
            width = prefMan.IMAGE_WIDTH_DEFAULT;
        }
        return width;

    }

    private void zipFiles(ArrayList<File> files)
    {

        String zipName = getZipName(!prefMan.isAlwaysAskForSaveName());
        if (!zipName.equals(CANCEL_DONE_COMMAND_STRING))
        {
            ZipFilesTask zip = new ZipFilesTask(files, zipName);
            zip.addPropertyChangeListener(this);
            zip.execute();
        } else
        {
            JOptionPane.showMessageDialog(null, "Files will not be zipped.", "Cancel Zip", JOptionPane.INFORMATION_MESSAGE);
            moveFilesToProcessedFolder();
        }
    }

    private String getZipName(boolean getDefault)
    {
        String fileSelected = saveProccessedFilesFolder.getAbsolutePath() + File.separator + "Shrunk Images  " + df.format(new Date(System.currentTimeMillis())) + ".zip";

        if (!getDefault)
        {
            int returnVal = fileChooserZip.showOpenDialog(this);
            if (returnVal != JFileChooser.CANCEL_OPTION)
            {
                if (returnVal == JFileChooser.APPROVE_OPTION)
                {

                    try
                    {
                        fileSelected = fileChooserZip.getSelectedFile().getAbsolutePath();
                    } catch (Exception ex)
                    {
                        JOptionPane.showMessageDialog(null, "Could not open file specified. Default location will be used.", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                } else
                {
                    JOptionPane.showMessageDialog(null, "Only a Zip can be selected. Default location will be used.", "Incorrect file type", JOptionPane.ERROR_MESSAGE);
                }
            } else
            {
                fileSelected = CANCEL_DONE_COMMAND_STRING;
                return fileSelected;
            }

            String[] fileSelectedSplit = fileSelected.split("\\.(?=[^\\.]+$)");

            if (fileSelectedSplit.length == 2)
            {
                if (!fileSelectedSplit[1].trim().equals("zip") && !fileSelectedSplit[1].trim().equals("ZIP"))
                {
                    JOptionPane.showMessageDialog(null, "Only a Zip can be selected. Default location will be used.", "Incorrect file type", JOptionPane.ERROR_MESSAGE);
                    fileSelected = saveProccessedFilesFolder.getAbsolutePath() + File.separator + "Shrunk Images  " + df.format(new Date(System.currentTimeMillis())) + ".zip";
                }
            } else
            {
                fileSelected += ".zip";
            }
        }
        return fileSelected;

    }

    private String getPDFName(boolean getDefault)
    {
        String fileSelected = saveProccessedFilesFolder.getAbsolutePath() + File.separator + MERGED_PDF_NAME + df.format(new Date(System.currentTimeMillis())) + ".pdf";

        if (!getDefault)
        {
            int returnVal = fileChooserPDF.showOpenDialog(this);
            if (returnVal != JFileChooser.CANCEL_OPTION)
            {
                if (returnVal == JFileChooser.APPROVE_OPTION)
                {

                    try
                    {
                        fileSelected = fileChooserPDF.getSelectedFile().getAbsolutePath();
                    } catch (Exception ex)
                    {
                        JOptionPane.showMessageDialog(null, "Could not open file specified. Default location will be used.", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                } else
                {
                    JOptionPane.showMessageDialog(null, "Only a PDF can be selected. Default location will be used.", "Incorrect file type", JOptionPane.ERROR_MESSAGE);
                }
            } else
            {
                fileSelected = CANCEL_DONE_COMMAND_STRING;
                return fileSelected;
            }

            String[] fileSelectedSplit = fileSelected.split("\\.(?=[^\\.]+$)");

            if (fileSelectedSplit.length == 2)
            {
                if (!fileSelectedSplit[1].trim().equals("pdf") && !fileSelectedSplit[1].trim().equals("PDF"))
                {
                    JOptionPane.showMessageDialog(null, "Only a PDF can be selected. Default location will be used.", "Incorrect file type", JOptionPane.ERROR_MESSAGE);
                    fileSelected = saveProccessedFilesFolder.getAbsolutePath() + File.separator + MERGED_PDF_NAME + df.format(new Date(System.currentTimeMillis())) + ".pdf";
                }
            } else
            {
                fileSelected += ".pdf";
            }
        }

        return fileSelected;
    }

    private FileFilter MyCustomPdfFilter()
    {
        return new MyCustomPdfFilter();
    }

    private FileFilter MyCustomZipFilter()
    {
        return new MyCustomZipFilter();
    }

    class MyCustomPdfFilter extends javax.swing.filechooser.FileFilter
    {

        @Override
        public boolean accept(File file)
        {
            // Allow only directories, or files with ".txt" extension
            return file.isDirectory() || file.getAbsolutePath().endsWith(".pdf");
        }

        @Override
        public String getDescription()
        {
            // This description will be displayed in the dialog,
            // hard-coded = ugly, should be done via I18N
            return "PDF Files (*.pdf)";
        }
    }

    class MyCustomZipFilter extends javax.swing.filechooser.FileFilter
    {

        @Override
        public boolean accept(File file)
        {
            // Allow only directories, or files with ".txt" extension
            return file.isDirectory() || file.getAbsolutePath().endsWith(".zip");
        }

        @Override
        public String getDescription()
        {
            // This description will be displayed in the dialog,
            // hard-coded = ugly, should be done via I18N
            return "Zip files (*.zip)";
        }
    }

    /**
     *
     * @author Mordechai
     */
    class ZipFilesTask extends SwingWorker<File, Void>
    {

        List<File> files;
        String filename;
        File zipfile;

        public ZipFilesTask(List<File> files, String filename)
        {
            this.files = files;
            this.filename = filename;
            zipfile = new File(filename);
        }

        @Override
        public File doInBackground() throws IOException
        {
            System.out.println("Start zip");
            showProgressBar(files.size(), "Zipping images...");

            int done = 0;

            byte[] buf = new byte[1024];

            // create the ZIP file
            ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipfile));
            for (File currentFile : files)
            {
                //we dont add pdf's to the zip file.
                if (!Thumbnail.getFileExtension(currentFile).equalsIgnoreCase("pdf"))
                {
                    try
                    {
                        FileInputStream in = new FileInputStream(currentFile);
                        // add ZIP entry to output stream
                        out.putNextEntry(new ZipEntry(currentFile.getName()));
                        // transfer bytes from the file to the ZIP file
                        int len;
                        while ((len = in.read(buf)) > 0)
                        {
                            out.write(buf, 0, len);
                        }
                        // complete the entry
                        out.closeEntry();
                        in.close();
                        //System.out.println("Done: " + done);
                        done++;
                        setScaledProgress(done);
                    } catch (IOException ex)
                    {
                        System.err.println("Could not zip " + currentFile.getName() + " - " + ex.getMessage());

                    } catch (Exception ex)
                    {
                        ex.printStackTrace();
                    }
                }
            }
            // complete the ZIP file
            out.close();
            System.out.println("Zip files succesfully created");
            return zipfile;

        }

        private void setScaledProgress(int completed)
        {
            setProgress(100 * completed / progressBar.getMaximum());
        }

        /*
         * Executed in event dispatching thread
         */
        @Override
        public void done()
        {
            hideProgressBar();
            moveFilesToProcessedFolder();
            try
            {
                if (!isCancelled())
                {
                    get();

                    showDialog(COMPRESS_ZIP_COMPLETE_MSG + "\n" + zipfile.getAbsolutePath(), COMPRESS_ZIP_COMPLETE_TITLE, JOptionPane.INFORMATION_MESSAGE);
                }
            } catch (ExecutionException e)
            {
                System.out.println("Exception: " + e.getCause());
                showDialog("Error zipping files. Please restart the application. Error: " + e.getCause(), "Error", JOptionPane.ERROR_MESSAGE);
            } catch (InterruptedException e)
            {
                System.out.println("Exception: " + e.getCause());
                showDialog("Error zipping files. Please restart the application. Error: " + e.getCause(), "Error", JOptionPane.ERROR_MESSAGE);

            }

        }

    }

    private void moveFilesToProcessedFolder()
    {
        File files[] = saveFolder.listFiles();
        ArrayList<Object> listOfFiles = new ArrayList<Object>(Arrays.asList(files));

        //remove pdf's used for merging images
        Iterator<Object> itr = listOfFiles.iterator();
        while (itr.hasNext())
        {
            File file = (File) itr.next();
            if (Thumbnail.getFileExtension(file).equalsIgnoreCase("pdf"))
            {
                file.delete();
                itr.remove();
            }

        }

        CopyTask ct = new CopyTask(listOfFiles, CopyToLocation.PROCCESSED_FOLDER);
        ct.addPropertyChangeListener(this);
        ct.execute();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        fileChooserPDF = new javax.swing.JFileChooser();
        fileChooserZip = new javax.swing.JFileChooser();
        mainLayer = new javax.swing.JLayeredPane();
        doneB = new javax.swing.JButton();
        imagePanelScrollPane = new javax.swing.JScrollPane();
        fileLayer = new javax.swing.JLayeredPane();
        removeAllB = new javax.swing.JButton();
        progressBar = new javax.swing.JProgressBar();
        progressBarLable = new javax.swing.JLabel();
        numFilesLabel = new javax.swing.JLabel();
        addAllInZipCheckBox = new javax.swing.JCheckBox();
        mergeImagesCheckBox = new javax.swing.JCheckBox();
        resizeImagesCheckBox = new javax.swing.JCheckBox();
        resizeToWidthL = new javax.swing.JLabel();
        resizeToWidthT = new javax.swing.JTextField();
        resizeToHeightL = new javax.swing.JLabel();
        resizeToHeightT = new javax.swing.JTextField();
        zipFilesCheckBox = new javax.swing.JCheckBox();
        jLabel1 = new javax.swing.JLabel();
        resizeMethodCombo = new javax.swing.JComboBox();
        openImagesFolderB = new javax.swing.JButton();
        addPageNumbersCheckBox = new javax.swing.JCheckBox();
        jMenuBar1 = new javax.swing.JMenuBar();
        openImagesFolderMenu = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        resetSettingsToDefualtMenuItem = new javax.swing.JMenuItem();
        viewPrefsMenu = new javax.swing.JMenuItem();

        fileChooserPDF.setFileFilter(MyCustomPdfFilter());
        fileChooserPDF.setDialogTitle("Select or create a PDF to save to.");
        fileChooserPDF.setFileSelectionMode(javax.swing.JFileChooser.FILES_AND_DIRECTORIES);
        fileChooserPDF.setApproveButtonText("Save");

        fileChooserZip.setFileFilter(MyCustomZipFilter());
        fileChooserZip.setDialogTitle("Select or create a Zip to compress to.");
        fileChooserZip.setFileSelectionMode(javax.swing.JFileChooser.FILES_AND_DIRECTORIES);
        fileChooserZip.setApproveButtonText("Save");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("DCCT Image and Document Processor");

        doneB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/done_icon.png"))); // NOI18N
        doneB.setText("Process Files");
        doneB.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
        doneB.setIconTextGap(10);
        doneB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                doneBActionPerformed(evt);
            }
        });

        imagePanelScrollPane.setPreferredSize(new java.awt.Dimension(700, 700));

        fileLayer.setPreferredSize(new java.awt.Dimension(700, 700));

        javax.swing.GroupLayout fileLayerLayout = new javax.swing.GroupLayout(fileLayer);
        fileLayer.setLayout(fileLayerLayout);
        fileLayerLayout.setHorizontalGroup(
            fileLayerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 700, Short.MAX_VALUE)
        );
        fileLayerLayout.setVerticalGroup(
            fileLayerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 700, Short.MAX_VALUE)
        );

        imagePanelScrollPane.setViewportView(fileLayer);

        removeAllB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/delete_icon.gif"))); // NOI18N
        removeAllB.setText("Remove All Files");
        removeAllB.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
        removeAllB.setIconTextGap(10);
        removeAllB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                removeAllBActionPerformed(evt);
            }
        });

        progressBarLable.setText("Loading images...");

        addAllInZipCheckBox.setText("Include PDF in ZIP");

        mergeImagesCheckBox.setText("Include Images in PDF");

        resizeImagesCheckBox.setText("Resize images");

        resizeToWidthL.setText("Resize images to width:");

        resizeToHeightL.setText("Resize images to height:");

        zipFilesCheckBox.setText("Zip images");

        jLabel1.setText("Resize method:");

        openImagesFolderB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/folder_icon.gif"))); // NOI18N
        openImagesFolderB.setText("Open Processed Files Folder");
        openImagesFolderB.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
        openImagesFolderB.setIconTextGap(10);
        openImagesFolderB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                openImagesFolderBActionPerformed(evt);
            }
        });

        addPageNumbersCheckBox.setText("Add page numbers to generated PDF");

        javax.swing.GroupLayout mainLayerLayout = new javax.swing.GroupLayout(mainLayer);
        mainLayer.setLayout(mainLayerLayout);
        mainLayerLayout.setHorizontalGroup(
            mainLayerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainLayerLayout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(removeAllB)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(doneB)
                .addGap(18, 18, 18)
                .addComponent(openImagesFolderB)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(mainLayerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(progressBarLable)))
            .addGroup(mainLayerLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mainLayerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mainLayerLayout.createSequentialGroup()
                        .addComponent(numFilesLabel)
                        .addGap(18, 18, 18)
                        .addGroup(mainLayerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(mainLayerLayout.createSequentialGroup()
                                .addComponent(mergeImagesCheckBox)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(resizeImagesCheckBox)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(zipFilesCheckBox)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(addPageNumbersCheckBox)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(addAllInZipCheckBox))
                            .addGroup(mainLayerLayout.createSequentialGroup()
                                .addGap(4, 4, 4)
                                .addComponent(resizeToWidthL)
                                .addGap(2, 2, 2)
                                .addComponent(resizeToWidthT, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(resizeToHeightL)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(resizeToHeightT, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(resizeMethodCombo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addComponent(imagePanelScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(21, 21, 21))
        );
        mainLayerLayout.setVerticalGroup(
            mainLayerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mainLayerLayout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(imagePanelScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 232, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mainLayerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(numFilesLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(mainLayerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(mergeImagesCheckBox)
                        .addComponent(resizeImagesCheckBox)
                        .addComponent(addAllInZipCheckBox)
                        .addComponent(zipFilesCheckBox)
                        .addComponent(addPageNumbersCheckBox)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mainLayerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(resizeToWidthL, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(mainLayerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(resizeToHeightL, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(resizeToHeightT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(resizeToWidthT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(resizeMethodCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(9, 9, 9)
                .addGroup(mainLayerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mainLayerLayout.createSequentialGroup()
                        .addComponent(progressBarLable, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5)
                        .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(mainLayerLayout.createSequentialGroup()
                        .addGroup(mainLayerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(mainLayerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(removeAllB, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(doneB, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(openImagesFolderB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())))
        );
        mainLayer.setLayer(doneB, javax.swing.JLayeredPane.DEFAULT_LAYER);
        mainLayer.setLayer(imagePanelScrollPane, javax.swing.JLayeredPane.DEFAULT_LAYER);
        mainLayer.setLayer(removeAllB, javax.swing.JLayeredPane.DEFAULT_LAYER);
        mainLayer.setLayer(progressBar, javax.swing.JLayeredPane.DEFAULT_LAYER);
        mainLayer.setLayer(progressBarLable, javax.swing.JLayeredPane.DEFAULT_LAYER);
        mainLayer.setLayer(numFilesLabel, javax.swing.JLayeredPane.DEFAULT_LAYER);
        mainLayer.setLayer(addAllInZipCheckBox, javax.swing.JLayeredPane.DEFAULT_LAYER);
        mainLayer.setLayer(mergeImagesCheckBox, javax.swing.JLayeredPane.DEFAULT_LAYER);
        mainLayer.setLayer(resizeImagesCheckBox, javax.swing.JLayeredPane.DEFAULT_LAYER);
        mainLayer.setLayer(resizeToWidthL, javax.swing.JLayeredPane.DEFAULT_LAYER);
        mainLayer.setLayer(resizeToWidthT, javax.swing.JLayeredPane.DEFAULT_LAYER);
        mainLayer.setLayer(resizeToHeightL, javax.swing.JLayeredPane.DEFAULT_LAYER);
        mainLayer.setLayer(resizeToHeightT, javax.swing.JLayeredPane.DEFAULT_LAYER);
        mainLayer.setLayer(zipFilesCheckBox, javax.swing.JLayeredPane.DEFAULT_LAYER);
        mainLayer.setLayer(jLabel1, javax.swing.JLayeredPane.DEFAULT_LAYER);
        mainLayer.setLayer(resizeMethodCombo, javax.swing.JLayeredPane.DEFAULT_LAYER);
        mainLayer.setLayer(openImagesFolderB, javax.swing.JLayeredPane.DEFAULT_LAYER);
        mainLayer.setLayer(addPageNumbersCheckBox, javax.swing.JLayeredPane.DEFAULT_LAYER);

        openImagesFolderMenu.setText("File");

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setText("Open Processed Files Folder");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                openImagesFolderMenuActionPerformed(evt);
            }
        });
        openImagesFolderMenu.add(jMenuItem1);

        jMenuBar1.add(openImagesFolderMenu);

        jMenu2.setText("Edit");

        resetSettingsToDefualtMenuItem.setText("Reset preferences to default");
        resetSettingsToDefualtMenuItem.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                resetSettingsToDefualtMenuItemActionPerformed(evt);
            }
        });
        jMenu2.add(resetSettingsToDefualtMenuItem);

        viewPrefsMenu.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_MASK));
        viewPrefsMenu.setText("Preferences...");
        viewPrefsMenu.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                viewPrefsMenuActionPerformed(evt);
            }
        });
        jMenu2.add(viewPrefsMenu);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mainLayer)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mainLayer, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void removeAllBActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_removeAllBActionPerformed
    {//GEN-HEADEREND:event_removeAllBActionPerformed
        if (numFiles > 0)
        {
            int dialogResult = JOptionPane.showConfirmDialog(null, "Do you really want to remove all files added for processing? \n (Origional files will not be effected)", "Warning", JOptionPane.YES_NO_OPTION);
            if (dialogResult == JOptionPane.YES_OPTION)
            {
                removeAllFiles();
            }
        }

    }//GEN-LAST:event_removeAllBActionPerformed

    private void removeAllFiles()
    {
        System.out.println("Removing existing files");

        thumbnails.clear();
        fileNames.clear();
        fileLayer.removeAll();
        updateNumFilesLabel(0);
        arrangeThumbnails();

        System.gc();
        File[] listOfFiles = saveFolder.listFiles();
        for (File file : listOfFiles)
        {
            if (file.isFile())
            {
                file.delete();
            }
        }
        isPdfToProccess = false;
    }

    private void doneBActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_doneBActionPerformed
    {//GEN-HEADEREND:event_doneBActionPerformed
        if (numFiles > 0)
        {

            if (isPdfToProccess || mergeImagesCheckBox.isSelected())
            {
                String PDFName = getPDFName(!prefMan.isAlwaysAskForSaveName());

                if (!PDFName.equals(CANCEL_DONE_COMMAND_STRING))
                {
                    ProccessFilesTask sit = new ProccessFilesTask(PDFName);
                    sit.addPropertyChangeListener(this);
                    sit.execute();
                } else
                {
                    JOptionPane.showMessageDialog(null, "Operation cancelled", "Cancel", JOptionPane.INFORMATION_MESSAGE);
                }
            } else
            {

                ProccessFilesTask sit = new ProccessFilesTask(false);
                sit.addPropertyChangeListener(this);
                sit.execute();
            }
        }
    }//GEN-LAST:event_doneBActionPerformed

    private void resetSettingsToDefualtMenuItemActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_resetSettingsToDefualtMenuItemActionPerformed
    {//GEN-HEADEREND:event_resetSettingsToDefualtMenuItemActionPerformed

        prefMan.setPreferencesToDefaults();
        loadPreferences();

    }//GEN-LAST:event_resetSettingsToDefualtMenuItemActionPerformed

    private void openImagesFolderBActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_openImagesFolderBActionPerformed
    {//GEN-HEADEREND:event_openImagesFolderBActionPerformed

        openImagesFolder();

    }//GEN-LAST:event_openImagesFolderBActionPerformed

    private void openImagesFolderMenuActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_openImagesFolderMenuActionPerformed
    {//GEN-HEADEREND:event_openImagesFolderMenuActionPerformed
        openImagesFolder();
    }//GEN-LAST:event_openImagesFolderMenuActionPerformed

    private void viewPrefsMenuActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_viewPrefsMenuActionPerformed
    {//GEN-HEADEREND:event_viewPrefsMenuActionPerformed
        new Preferences().setVisible(true);

    }//GEN-LAST:event_viewPrefsMenuActionPerformed

    private void openImagesFolder()
    {
        try
        {
            Desktop.getDesktop().open(saveProccessedFilesFolder);
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(null, "Folder could not be opened. Restart application.", "Error", JOptionPane.ERROR_MESSAGE);
            System.err.println("Could not open folder: " + saveProccessedFilesFolder);
        }
    }

    private BufferedImage toBufferedImage(Image image, int imageType)//cunt
    {
        BufferedImage bimage = new BufferedImage(image.getWidth(null), image.getHeight(null), imageType);

        // Draw the image on to the buffered image
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(image, 0, 0, null);
        bGr.dispose();

        return bimage;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() ->
        {
            new Home().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox addAllInZipCheckBox;
    private javax.swing.JCheckBox addPageNumbersCheckBox;
    private javax.swing.JButton doneB;
    private javax.swing.JFileChooser fileChooserPDF;
    private javax.swing.JFileChooser fileChooserZip;
    private javax.swing.JLayeredPane fileLayer;
    private javax.swing.JScrollPane imagePanelScrollPane;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JLayeredPane mainLayer;
    private javax.swing.JCheckBox mergeImagesCheckBox;
    private javax.swing.JLabel numFilesLabel;
    private javax.swing.JButton openImagesFolderB;
    private javax.swing.JMenu openImagesFolderMenu;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JLabel progressBarLable;
    private javax.swing.JButton removeAllB;
    private javax.swing.JMenuItem resetSettingsToDefualtMenuItem;
    private javax.swing.JCheckBox resizeImagesCheckBox;
    private javax.swing.JComboBox resizeMethodCombo;
    private javax.swing.JLabel resizeToHeightL;
    private javax.swing.JTextField resizeToHeightT;
    private javax.swing.JLabel resizeToWidthL;
    private javax.swing.JTextField resizeToWidthT;
    private javax.swing.JMenuItem viewPrefsMenu;
    private javax.swing.JCheckBox zipFilesCheckBox;
    // End of variables declaration//GEN-END:variables
}
