/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import org.imgscalr.Scalr.Mode;

/**
 *
 * @author Mordechai
 */
public class ResizeMethodComboItem
{

    private String key;
    private Mode mode;

    public ResizeMethodComboItem(String key, Mode mode)
    {
        this.key = key;
        this.mode = mode;
    }

    public String getKey()
    {
        return key;
    }

    public Mode getMode()
    {
        return mode;
    }
    
    @Override
    public String toString()
    {
        return key;
    }

}
