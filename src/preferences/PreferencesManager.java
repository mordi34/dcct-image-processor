/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preferences;

import GUI.Home;
import java.io.File;
import java.util.prefs.Preferences;

/**
 *
 * @author Mordechai
 */
public class PreferencesManager
{

    private final Preferences prefs;

    private final String MERGE_IMAGES_KEY = "MERGE_IMAGES_KEY";
    private final String RESIZE_IMAGES_KEY = "RESIZE_IMAGES_KEY";
    private final String ADD_ALL_TO_ZIP_KEY = "ADD_ALL_TO_ZIP_KEY";
    private final String ZIP_FILES_KEY = "ZIP_FILES_KEY";
    private final String ALWAYS_ASK_FOR_SAVE_NAME_KEY = "ALWAYS_ASK_FOR_SAVE_NAME_KEY";
    private final String ADD_PAGE_NUMBERS_KEY = "ADD_PAGE_NUMBERS";

    private final String IMAGE_HEIGHT_KEY = "IMAGE_HEIGHT";
    private final String IMAGE_WIDTH_KEY = "IMAGE_WIDTH";

    private final String SAVE_DIRECTORY_KEY = "SAVE_DIRECTORY_KEY";
    private final String PROCESSED_DIRECTORY_KEY = "PROCESSED_DIRECTORY_KEY";
    private final String PROCESSED_FOLDER_NAME_KEY = "PROCESSED_FOLDER_NAME_KEY";
    private final String SAVE_FOLDER_NAME_KEY = "SAVE_FOLDER_NAME_KEY";

    /**
     * Have default values to allow for reset to defaults midsession. Change the
     * other variables as changes are made to allow for user to set settings
     * even in event where prefs cannot be saved. These will not be persistent
     * but at least user can still change settings in a session.
     */
    public final boolean MERGE_IMAGES_DEFAULT = false;
    public final boolean RESIZE_IMAGES_DEFAULT = true;
    public final boolean ADD_ALL_TO_ZIP_DEFAULT = true;
    public final boolean ZIP_FILES_DEFAULT = true;
    public final boolean ALWAYS_ASK_FOR_SAVE_NAME_DEFAULT = true;
    public final boolean ADD_PAGE_NUMBERS_DEFAULT = true;

    public final int IMAGE_WIDTH_DEFAULT = 1024;
    public final int IMAGE_HEIGHT_DEFAULT = 768;

    private final String SAVE_FOLDER_NAME_DEFAULT = "DCCT Files For Processing";
    private final String PROCESSED_FOLDER_NAME_DEFUALT = "DCCT Processed Files";
    /**
     * System dependant operation here. Use user.home for non-windows systems.
     */
    //  public static String SAVE_DIRECTORY = System.getProperty("user.home");
    private final String SAVE_DIRECTORY_DEFAULT = System.getenv("APPDATA");
    private final String PROCESSED_DIRECTORY_DEFAULT = System.getenv("APPDATA");

    /**
     * Values to be used as prefs.
     */
    private boolean mergeImages;
    private boolean resizeImages;
    private boolean addAllToZip;
    private boolean zipFiles;
    private boolean alwaysAskForSaveName;
    private boolean addPageNumbers;

    private int imageWidth;
    private int imageHeight;

    private String saveFolderName;
    private String processedFileName;
    private String saveDirectory;
    private String processedDirectory;

    public PreferencesManager()
    {
        prefs = Preferences.userNodeForPackage(this.getClass());

        mergeImages = MERGE_IMAGES_DEFAULT;
        resizeImages = RESIZE_IMAGES_DEFAULT;
        addAllToZip = ADD_ALL_TO_ZIP_DEFAULT;
        zipFiles = ZIP_FILES_DEFAULT;
        alwaysAskForSaveName = ALWAYS_ASK_FOR_SAVE_NAME_DEFAULT;
        addPageNumbers = ADD_PAGE_NUMBERS_DEFAULT;

        imageWidth = IMAGE_WIDTH_DEFAULT;
        imageHeight = IMAGE_HEIGHT_DEFAULT;

        saveFolderName = SAVE_FOLDER_NAME_DEFAULT;
        processedFileName = PROCESSED_FOLDER_NAME_DEFUALT;
        saveDirectory = SAVE_DIRECTORY_DEFAULT;
        processedDirectory = PROCESSED_DIRECTORY_DEFAULT;

    }

    public void setPreferencesToDefaults()
    {
        setAddAllToZipPref(ADD_ALL_TO_ZIP_DEFAULT);
        setMergeImagesPref(MERGE_IMAGES_DEFAULT);
        setResizeImagesPref(RESIZE_IMAGES_DEFAULT);
        setZipFilesPref(ZIP_FILES_DEFAULT);
        setAlwaysAskForSaveName(ALWAYS_ASK_FOR_SAVE_NAME_DEFAULT);
        setAddPageNumbers(ADD_PAGE_NUMBERS_DEFAULT);

        setImageHeightPref(IMAGE_HEIGHT_DEFAULT);
        setImageWidthPref(IMAGE_WIDTH_DEFAULT);

        setSaveDirectoryPref(SAVE_DIRECTORY_DEFAULT);
        setSaveDirectoryPref(PROCESSED_DIRECTORY_DEFAULT);
        setSaveFolderNamePref(SAVE_FOLDER_NAME_DEFAULT);
        setProcessedFileNamePref(PROCESSED_FOLDER_NAME_DEFUALT);

        setSaveFolderToCurrentPrefs();

        setProccessedFolderTOCurrentPrefs();

        System.out.println("Default prefs saved.");
    }

    public void setPreferences(boolean mergeImages, boolean resizeImages, boolean addAllToZip, boolean zipFiles, boolean addPageNumbers, int imageWidth, int imageHeight,
            String saveDir, String processedDir, String saveFolderName, String processedFolderName)
    {
        setAddAllToZipPref(addAllToZip);
        setMergeImagesPref(mergeImages);
        setResizeImagesPref(resizeImages);
        setZipFilesPref(zipFiles);
        setAddPageNumbers(addPageNumbers);

        setImageHeightPref(imageHeight);
        setImageWidthPref(imageWidth);

        setSaveDirectoryPref(saveDir);
        setProcessedDirectoryPref(processedDir);
        setSaveFolderNamePref(saveFolderName);
        setProcessedFileNamePref(processedFolderName);
    }

    public void setPreferences(boolean mergeImages, boolean resizeImages, boolean addAllToZip, boolean zipFiles, boolean addPageNumbers,int imageWidth, int imageHeight)
    {
        setAddAllToZipPref(addAllToZip);
        setMergeImagesPref(mergeImages);
        setResizeImagesPref(resizeImages);
        setZipFilesPref(zipFiles);
        setAddPageNumbers(addPageNumbers);

        setImageHeightPref(imageHeight);
        setImageWidthPref(imageWidth);

    }

    public void setSaveFolderToCurrentPrefs()
    {

        File saveFolder = new File(SAVE_DIRECTORY_DEFAULT + File.separator + SAVE_FOLDER_NAME_DEFAULT);
        if (!saveFolder.exists())
        {
            saveFolder.mkdir();
        }
        Home.setSaveFile(saveFolder);
    }

    public void setProccessedFolderTOCurrentPrefs()
    {
        File processedFolder = new File(PROCESSED_DIRECTORY_DEFAULT + File.separator + PROCESSED_FOLDER_NAME_DEFUALT);
        if (!processedFolder.exists())
        {
            processedFolder.mkdir();
        }
        Home.setProcessedFilesFile(processedFolder);
    }

    public void setNewSaveFolder(String directory, String folderName)
    {
        setSaveDirectoryPref(directory);
        setSaveFolderNamePref(folderName);
        setSaveFolderToCurrentPrefs();
    }

    public void setNewProccessedFolder(String directory, String folderName)
    {
        setProcessedDirectoryPref(directory);
        setProcessedFileNamePref(folderName);
        setProccessedFolderTOCurrentPrefs();
    }

    public void setNewSaveFolder(File folder)
    {
        setNewSaveFolder(folder.getParent(), folder.getName());
    }

    public void setNewProccessedFolder(File folder)
    {
        setNewProccessedFolder(folder.getParent(), folder.getName());
    }

    public String getSaveFolderName()
    {
        return prefs.get(SAVE_FOLDER_NAME_KEY, saveFolderName);
    }

    public String getProcessedFileName()
    {
        return prefs.get(PROCESSED_FOLDER_NAME_KEY, processedFileName);
    }

    public String getSaveDirectory()
    {
        return prefs.get(SAVE_DIRECTORY_KEY, saveDirectory);
    }

    public String getProcessedDirectory()
    {
        return prefs.get(PROCESSED_DIRECTORY_KEY, processedDirectory);
    }

    public boolean isMergeImagesPref()
    {
        return prefs.getBoolean(MERGE_IMAGES_KEY, mergeImages);
    }

    public boolean isResizeImagesPref()
    {
        return prefs.getBoolean(RESIZE_IMAGES_KEY, resizeImages);
    }

    public boolean isAddAllToZipPref()
    {
        return prefs.getBoolean(ADD_ALL_TO_ZIP_KEY, addAllToZip);
    }

    public boolean isZipFilesPref()
    {
        return prefs.getBoolean(ZIP_FILES_KEY, zipFiles);
    }

    public boolean isAlwaysAskForSaveName()
    {
        return prefs.getBoolean(ALWAYS_ASK_FOR_SAVE_NAME_KEY, alwaysAskForSaveName);
    }

    public boolean isAddPageNumbers()
    {
        return prefs.getBoolean(ADD_PAGE_NUMBERS_KEY, addPageNumbers);
    }

    public int getImageWidthPref()
    {
        return prefs.getInt(IMAGE_WIDTH_KEY, imageWidth);
    }

    public int getImageHeightPref()
    {
        return prefs.getInt(IMAGE_HEIGHT_KEY, imageHeight);
    }

    public void setMergeImagesPref(boolean mergeImages)
    {
        this.mergeImages = mergeImages;
        prefs.putBoolean(MERGE_IMAGES_KEY, mergeImages);
    }

    public void setZipFilesPref(boolean zipFiles)
    {
        this.zipFiles = zipFiles;
        prefs.putBoolean(ZIP_FILES_KEY, zipFiles);
    }

    public void setResizeImagesPref(boolean resizeImages)
    {
        this.resizeImages = resizeImages;
        prefs.putBoolean(RESIZE_IMAGES_KEY, resizeImages);
    }

    public void setAddAllToZipPref(boolean addAllToZip)
    {
        this.addAllToZip = addAllToZip;
        prefs.putBoolean(ADD_ALL_TO_ZIP_KEY, addAllToZip);
    }

    public void setAlwaysAskForSaveName(boolean alwaysAskForSaveName)
    {
        this.alwaysAskForSaveName = alwaysAskForSaveName;
        prefs.putBoolean(ALWAYS_ASK_FOR_SAVE_NAME_KEY, alwaysAskForSaveName);
    }
    
    public void setAddPageNumbers(boolean addPageNumbers)
    {
        this.addPageNumbers = addPageNumbers;
        prefs.putBoolean(ADD_PAGE_NUMBERS_KEY, addPageNumbers);
    }

    public void setImageWidthPref(int imageWidth)
    {
        this.imageWidth = imageWidth;
        prefs.putInt(IMAGE_WIDTH_KEY, imageWidth);
    }

    public void setImageHeightPref(int imageHeight)
    {
        this.imageHeight = imageHeight;
        prefs.putInt(IMAGE_HEIGHT_KEY, imageHeight);
    }

    public void setSaveFolderNamePref(String saveFolderName)
    {
        this.saveFolderName = saveFolderName;
        prefs.put(SAVE_FOLDER_NAME_KEY, saveFolderName);
    }

    public void setProcessedFileNamePref(String saveProcessedFileName)
    {
        this.processedFileName = saveProcessedFileName;
        prefs.put(PROCESSED_FOLDER_NAME_KEY, saveProcessedFileName);
    }

    public void setSaveDirectoryPref(String saveDirectory)
    {
        this.saveDirectory = saveDirectory;
        prefs.put(SAVE_DIRECTORY_KEY, saveDirectory);
    }

    public void setProcessedDirectoryPref(String processedDirectory)
    {
        this.processedDirectory = processedDirectory;
        prefs.put(PROCESSED_DIRECTORY_KEY, processedDirectory);
    }

}
